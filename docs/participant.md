# Participant

A Participant is a Legal Person or Natural Person, which is identified, onboarded and has a Gaia-X Self-Description. Instances of Participant neither being a legal nor a natural person are prohibited.

```mermaid
classDiagram

class Participant {
    <<abstract>>
}

Participant <|-- LegalPerson
Participant <|-- NaturalPerson
```

The Architecture Document defines three roles a Participant can have within the Gaia-X Ecosystem (Provider, Consumer, and Federator). These are not yet part of Trust Framework and are to be defined in future releases.

## Legal person

For legal person the attributes are

| Attribute                                                           | Cardinality | Trust Anchor | Comment                                           |
|---------------------------------------------------------------------|------|-----------|---------------------------------------------------|
| `registrationNumber`                                                | 1    | State | Country's registration number, which identifies one specific entity. |
| `headquarterAddress`.[`countryCode`](https://schema.org/addressCountry) | 1    | State | Physical location of head quarter in [ISO 3166-2](https://www.iso.org/standard/72483.html) alpha2, alpha-3 or numeric format. |
| `legalAddress`.[`countryCode`](https://schema.org/addressCountry)       | 1    | State | Physical location of legal registration in [ISO 3166-2](https://www.iso.org/standard/72483.html) alpha2, alpha-3 or numeric format. |
| [`parentOrganisation[]`](https://schema.org/parentOrganization)     | 0..* | State | A list of direct `participant` that this entity is a subOrganization of, if any. |
| [`subOrganisation[]`](https://schema.org/subOrganization)           | 0..* | State | A list of direct `participant` with an legal mandate on this entity, e.g., as a subsidiary. |
| `termsAndConditions`                                                | 1    | State | SHA512 of the Generic Terms and Conditions for Gaia-X Ecosystem as defined below |

### registrationNumber

The list of valid entity `registrationNumber` type are described below:

| Attribute | Comment                                           |
|-----------|---------------------------------------------------|
| [`local`](https://schema.org/taxID)   | the state issued company number                   |
| [`EUID`](https://schema.org/Text)    | the [European Unique Identifier (EUID)](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32020R2244&from=en#d1e1428-3-1) for business located in the [European Economic Area](https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Glossary:European_Economic_Area_(EEA)), Iceland, Liechtenstein or Norway and registered in the Business Registers Interconnection System ([BRIS](https://e-justice.europa.eu/content_business_registers_at_european_level-105-en.do)). This number can be found via the [EU Business registers portal](https://e-justice.europa.eu/content_find_a_company-489-en.do) |
| [`EORI`](https://schema.org/Text)    | the [Economic Operators Registration and Identification number (EORI)](https://ec.europa.eu/taxation_customs/business/customs-procedures-import-and-export/customs-procedures/economic-operators-registration-and-identification-number-eori_en). |
| [`vatID`](https://schema.org/vatID)   | the VAT identification number. |
| [`leiCode`](https://schema.org/leiCode) | Unique LEI number as defined by <https://www.gleif.org>. |

**Consistency rules**

- if `EORI` is provided, the number will be verified against the European Commission [API](https://ec.europa.eu/taxation_customs/dds2/eos/validation/services/validation?wsdl).
- if `leiCode` is provided, the number will be verified against the Global Legal Entity Identifier (GLEIF) [API](https://www.gleif.org/en/lei-data/gleif-api?cachepath=fr%2Flei-data%2Fgleif-api)
- if `local` is provided, the number will be verified with `headquarterAddress.countryCode` against the OpenCorporate [API](https://api.opencorporates.com/).
- if `vatID` is provided and `headquarterAddress.countryCode` belongs to the European member states or North Ireland, the number will be checked against the VAT Information Exchange System (VIES) [API](https://ec.europa.eu/taxation_customs/vies/checkVatTestService.wsdl)
- if several numbers are provided, the information provided by each number must be consistent.

### Gaia-X Ecosystem Terms and Conditions

```text
The PARTICIPANT signing the Self-Description agrees as follows:
- to update its descriptions about any changes, be it technical, organisational, or legal - especially but not limited to contractual in regards of the indicated attributes present in the descriptions.
- wrongful statements will reflect a breach of contract and may cumulate to unfair competitive behaviour.
- in cases of systematic and deliberate misrepresentations, Gaia-X Association is, without prejudice to claims and rights under the applicable law, entitled to take actions as defined in this document Architecture document - Operation model chapter - Self-Description Remediation section.

Alongside, the PARTICIPANT signing the Self-Description is aware and accepts that:
- the SERVICE OFFERING will be delisted where Gaia-X Association becomes aware of any inaccurate statements in regards of the SERVICE OFFERING which result in a non-compliance with the Trust Framework and Policy Rules document.
```

## Natural person

To be defined in a future release.

<!--
TODO: JUNE 2022
which comes from ISO 25237: real human being as opposed to a legal person which may be a private or public organization

For natural person, the attributes are

| Version | Attribute                                      | Cardinality | Signed by | Comment                                           |
|---------|------------------------------------------------|-------|-----------------|---------------------------------------------------|
| 1.x     | `taxAddress[].`[`country`](https://schema.org/addressCountry) | 0..*  | State | location in [ISO 3166-1](https://www.iso.org/iso-3166-country-codes.html) alpha2, alpha-3 or numeric format. |
| 1.x     | `livingAddress[].`[`country`](https://schema.org/addressCountry) | 0..*  | State | Physical location in [ISO 3166-1](https://www.iso.org/iso-3166-country-codes.html) alpha2, alpha-3 or numeric format. |

-->

<!--
## Provider

A Provider aggregates with Participant.

| Version | Attribute          | Card. | Comment                                           |
|---------|--------------------|-------|---------------------------------------------------|
| x.x     | `to_be_defined`    | 1     | to be defined | mandatory public information      |

-->
