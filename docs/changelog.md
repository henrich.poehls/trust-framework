# Changelog

## 2022 June release (22.06)

**General update**

- replaced ISO 3166-1 country code by [ISO 3166-2](https://en.wikipedia.org/wiki/ISO_3166-2) identifying the principal subdivisions (e.g., provinces or states) inside a country.
- adopted a global versioning system for both the document and the attributes: `YY.MM[.PATCH]`. (Removed `version` per attribute)

**About `LegalParticipant`**

- refactored the mandatory `registrationNumber` attribute for `LegalParticipant`:
  - included several possible formats: `local`, `EUID`, `EORI`, `vatID`, `leiCode`.
  - added `registrationNumber` validation constraints using 3rd party external [Trusted Data Sources](https://docs.gaia-x.eu/technical-committee/architecture-document/22.04/operating_model/#trust-anchors).
- added mandatory signature of mandatory Gaia-X Terms & Conditions.

**About `ServiceOffering`**

- added optional `dataProtectionRegime[]` attribute for a generic approach across various existing data protection regimes. The list will be completed over time.
- added mandatory `dataExport[]` attribute for information on the process to request and export personal and non-personal data out of Service Offering.

**About `Instantiated Virtual Resource`**

- renamed `endpoint` by the generic network layer agnostic term `serviceAccessPoint`

## 2022 April release (22.04)

- First release of the Trust Framework document.
- contains basic `Participant`, `ServiceOffering` and `Resource` modeling.
