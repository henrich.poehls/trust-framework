# Service Offering

While a more complete description of the service offerings taxonomy is being defined by the Service Characteristics Working Group and will be integrated in a later version of this  document, some services must already be specified to enable building a common governance across ecosystems.

```mermaid
flowchart BT
    so[Service Offering]
    dataset[Data management]
    dataset --> so

    dec[Data Exchange Connector]
    dec --> dataset

    wallet[Verifiable Credential Wallet]
    wallet --> dataset

    network[Network]
    network --> so

    inter[Interconnection]
    inter --> network
``` 

## Data exchange connector service


A `data connector` offering, including but not limited to, a service bus, a messaging or an event streaming platform, must fulfil those requirements:

<!-- requirements from the DSBC position paper https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf -->

| Attribute               | Card. | Trust Anchor | Comment                                         |
|-------------------------|-------|--------------|-------------------------------------------------|
| `physicalResource[]`    | 1..*  | State        | a list of `resource`s with information of where is the data located. |
| `virtualResource[]`     | 1..*  | State        | a list of `resource` with information of who owns the data. |
| `policies[]`            | 1..*  | State        | a list of `policy` expressed using a DSL used by the data connector policy engine leveraging Gaia-X Self-Descriptions as both data inputs and policy inputs |


## Verifiable Credential Wallet

A `wallet` enables to store, manage, present Verifiable Credentials:
<!-- requirements from the DSBC position paper https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf -->

| Attribute                            | Card. | Trust Anchor | Comment                                         |
|--------------------------------------|-------|--------------|-------------------------------------------------|
| `verifiableCredentialExportFormat[]` | 1..*  | State        | a list of machine readable formats used to export verifiable credentials. |
| `privateKeyExportFormat[]`           | 1..*  | State        | a list of machine readable format used to export private keys. |

## Interconnection

An `interconnection` enables a mutual connection between two or more elements.

| Attribute                               | Card. | Trust Anchor | Comment                                         |
|-----------------------------------------|-------|--------------|-------------------------------------------------|
| `location[].country`                    | 2..*  | State        | a list of physical locations in ISO 3166-1 alpha2, alpha-3 or numeric format with at least the both ends of the connection. |


