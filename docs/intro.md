# Technical Prelude

Each table of this document represents a schema for a JSON or, equivalently, YAML document.
The tables define the terms to be used in *claims* according to the Verifiable Credentials Data Model.
Behind the scenes, there shall be a JSON-LD context[^jsonld], which maps all terms such as attribute names to full URIs for interoperability according to the RDF data model, as is required by the Verifiable Credentials Data Model.
This context will be published together with the machine-executable implementation of the Trust Framework at https://w3id.org/gaia-x/core/ with the release of this specification.
[^jsonld]: https://www.w3.org/TR/json-ld11/

## Attribute type

### Address

Address refers to an object such as [`address`](https://schema.org/PostalAddress) object which contains a `country` attribute.

All `country` attributes are in the [ISO 3166-1](https://www.iso.org/iso-3166-country-codes.html) alpha2, alpha-3 or numeric format.

## JSONPath

Attributes can also express a JSONPath.

Ex: `location[].country` represents the path to a `country` attribute inside an array of `location` objects, like

```json
{
   "location":[
      {
         "zipCode":"75001",
         "city":"Paris",
         "country":"FR"
      }
   ]
}
```

## Cardinality

| Cardinality | Relationship      |
|-------------|-------------------|
| 0..1        | Optional          |
| 0..*        | Optional to Many  |
| 1           | Mandatory         |
| 1..N        | Mandatory to N    |
| 1..*        | Mandatory to Many |

## Trust Anchor

The `Trust Anchor` column contains the information on the Trust Anchor which issued the root keypair in the chain of keypair used to signed the claim.

```mermaid
flowchart LR
    ta[Trust Anchor]

    subgraph holder1 [Holder ...]
        k1[keypair]
        direction TB
    end

    ta -- issues --> k1

    subgraph holder2 [Holder N]
        k2[keypair]
        direction TB
    end

    holder1 -- issues --> k2

    holder2 -- signs --> claim
```

