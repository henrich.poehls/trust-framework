# Services & Resources

Here is the main model for service composition, also included in the Gaia-X Architecture document.

A `Service Offering` can be associated with other `Service Offering`s.

```mermaid
classDiagram
    ServiceOffering o-- Resource: aggregationOf
    Resource o-- Resource: aggregationOf

class Resource{
    <<abstract>>
}

    Resource <|-- "subClassOf" PhysicalResource: 
    Resource <|-- "subClassOf" VirtualResource
    VirtualResource "1" <-- "*" InstantiatedVirtualResource : instanceOf

    ServiceOffering "1" ..> "1" Participant : providedBy

    PhysicalResource "1" ..> "0..*" Participant : ownedBy
    PhysicalResource "1" ..> "0..*" Participant : manufacturedBy
    PhysicalResource "1" ..> "1..*" Participant : maintainedBy

    VirtualResource "1" ..> "1..*" Participant : copyrightOwnedBy

    InstantiatedVirtualResource --> "1" Resource : hostedOn

    InstantiatedVirtualResource <..> ServiceInstance : equivalent to
    InstantiatedVirtualResource "1" ..> "1..*" Participant : tenantOwnedBy
    InstantiatedVirtualResource "1" ..> "1..*" Participant : maintainedBy
``` 

## Service offering

This is the generic format for all service offerings

| Attribute              | Card. | Trust Anchor | Comment                                         |
|------------------------|-------|--------------|-------------------------------------------------|
| `providedBy`           | 1     | State        | a resolvable link to the `participant` self-description providing the service    |
| `aggregationOf[]`      | 0..*  | State        | a resolvable link to the `resources` self-description related to the service and that can exist independently of it. |
| `dependsOn[]`          | 0..*  | State        | a resolvable link to the `service offering` self-description related to the service and that can exist independently of it. |
| `termsAndConditions[]` | 1..*  | State        | a resolvable link to the Terms and Conditions appling to that service. |
| `policies[]`           | 0..*  | State        | a list of `policy` expressed using a DSL (e.g., Rego or ODRL) |
| `dataProtectionRegime[]`| 0..*  | State        | a list of data protection regime from the list available below |
| `dataExport[]`         | 1..*  | State        | list of methods to export data out of the service |

**termsAndConditions structure**

| Attribute              | Card. | Trust Anchor | Comment                                         |
|------------------------|-------|--------------|-------------------------------------------------|
| `URL`                  | 1     | State        | a resolvable link to document    |
| `hash`                 | 1     | State        | sha256 hash of the above document. |

**dataExport structure**

The purpose is to enable the participant ordering the service to assess the feasability to export personal and non-personal data out of the service.

| Attribute     | Card. | Trust Anchor | Comment                                         |
|---------------|-------|--------------|-------------------------------------------------|
| `requestType` | 1     | State        | the mean to request data retreival: `API`, `email`, `webform`, <br> `unregisteredLetter`, `registeredLetter`, `supportCenter` |
| `accessType`  | 1     | State        | type of data support: `digital`, `physical` |
| `formatType`  | 1     | State        | type of Media Types (formerly known as MIME types) as defined by the [IANA](https://www.iana.org/assignments/media-types/media-types.xhtml). |

**Data Protection Regime**

To enable interoperability and automate policy negociation, the Gaia-X association strongly advocates to use the list of data protection regimes listed in the [Gaia-X Registry](https://registry.gaia-x.eu)

Non exclusive list of Data Protection regimes:

- `GDPR2016`: [General Data Protection Regulation](https://eur-lex.europa.eu/eli/reg/2016/679/oj) / EEA
- `LGPD2019`: [General Personal Data Protection Law](http://www.planalto.gov.br/ccivil_03/_ato2015-2018/2018/lei/l13709.htm) (_Lei Geral de Proteção de Dados Pessoais_) / BRA
- `PDPA2012`: [Personal Data Protection Act 2012](https://sso.agc.gov.sg/Act/PDPA2012) / SGP
- `CCPA2018`: [California Consumer Privacy Act](https://oag.ca.gov/privacy/ccpa) / US-CA
- `VCDPA2021`: [Virginia Consumer Data Protection Act](https://lis.virginia.gov/cgi-bin/legp604.exe?212+ful+CHAP0036+pdf) / US-VA

**Consistency rules**

- the keys used to sign a SERVICE OFFERING description and the `providedBy` PARTICIPANT description should be from the same keychain.

## Resource

A resource that may be aggregated in a Service Offering or exist independently of it.

| Attribute               | Card. | Trust Anchor | Comment                                         |
|-------------------------|-------|--------------|-------------------------------------------------|
| `aggregationOf[]`       | 0..*  | State        | `resources` related to the resource and that can exist independently of it. |

### Physical Resource

A Physical Resouce inherits from a Resource.  
A Physical resource is, but not limited to, a datacenter, a baremetal service, a warehouse, a plant. Those are entities that have a weight and position in physical space.

| Attribute              | Card. | Trust Anchor | Comment                                     |
|------------------------|-------|--------------|---------------------------------------------|
| `maintainedBy[]`       | 1..*  | State        | a list of `participant` maintaining the resource in operational condition and thus having physical access to it. |
| `ownedBy[]`            | 0..*  | State        | a list of `participant` owning the resource. |
| `manufacturedBy[]`     | 0..*  | State        | a list of `participant` manufacturing the resource. |
| `locationAddress[].country` | 1..*  | State   | a list of physical location in ISO 3166-1 alpha2, alpha-3 or numeric format. |
| `location[].gps`       | 0..*  | State        | a list of physical GPS in [ISO 6709:2008/Cor 1:2009](https://en.wikipedia.org/wiki/ISO_6709) format. |

### Virtual Resource

A Virtual Resource inherits from a Resource.  
A Virtual resource is a resource describing recorded information such as, and not limited to, a dataset, a software, a configuration file, an AI model. Special sub-classes of Virtual Resource are `SoftwareResource` and `DataResource`.

```mermaid
classDiagram

VirtualResource <|-- SoftwareResource
VirtualResource <|-- DataResource 

```

| Attribute            | Card. | Trust Anchor | Comment                                   |
|----------------------|-------|--------------|-------------------------------------------|
| `copyrightOwnedBy[]` | 1..*  | State        | A list of copyright owners either as a free form string or `participant` URIs from which Self-Descriptions can be retrieved. A copyright owner is a person or organization that has the right to exploit the resource. Copyright owner does not necessarily refer to the author of the resource, who is a natural person and may differ from copyright owner. |
| `license[]`          | 1..*  | State        | A list of [SPDX](https://github.com/spdx/license-list-data/tree/master/jsonld) license identifiers or URL to license document |

### Instantiated Virtual Resource

An Instantiated Virtual Resource is an instance from a Virtual Resource.  
An Instantiated Virtual resource is a running resource exposing endpoints such as, and not limited to, a running process, an online API, a network connection, a virtual machine, a container, an operating system. 

| Attribute            | Card. | Trust Anchor | Comment                                   |
|----------------------|-------|--------------|-------------------------------------------|
| `maintainedBy[]`     | 1..*  | State        | a list of `participant` maintaining the resource in operational condition. |
| `hostedOn`           | 1     | State        | a `resource` where the process is running, being executed on. |
| `instanceOf`         | 1     | State        | a `virtual resource` (normally a `software` resource) this process is an instance of. |
| `tenantOwnedBy[]`    | 1..*  | State        | a list of `participant` with contractual relation with the resource. |
| `serviceAccessPoint[]`              | 1..*  | State        | a list of [Service Acccess Point](https://en.wikipedia.org/wiki/Service_Access_Point) which can be an endpoint as a mean to access and interact with the resource |

<!--
| 1.x     | `virtualLocation[]`  | 1..*  | State        | a list of location such an Availability Zone, network segment, IP range, AS number, ... (format to be specified in a separated table) |
-->
